package org.epam.Exeptions;

import java.util.ArrayList;
import java.util.List;

/**
 *  Class not found page site.
 */

public class NotFound {
    /** List not found page. */
    static List<Integer> pageNumbers = new ArrayList<>();

    /** Add page for list. */
    public static void addNumberPage(int number) {
        if(!checkNumberPage(number)) {
            pageNumbers.add(number);
        }
    }

    public static void delNumberPage(int number) {
        if(checkNumberPage(number)) {
            pageNumbers.remove(number);
        }
    }

    /**  Get list not found */
    public static void listNotFound() {
        if(pageNumbers.size() == 0) {
            System.out.println("NotFound # List clear.");
            return;
        }

        System.out.println("NotFound # Pages not found:");
        for(int i : pageNumbers){
            System.out.println("Number = " + i);
        }
    }

    /**  Check page for exeptoins */
    public static boolean checkNumberPage(int num) {
        for (int i : pageNumbers){
            if(num == i) return true;
        }
        return false;
    }
}
