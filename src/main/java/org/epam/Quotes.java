package org.epam;

import org.epam.Exeptions.NotFound;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Quotes extends NotFound {
    /** Number page */
    private int numberPage;
    private String url = "https://bash.im/quote/";

    /** Constructor */
    public Quotes () {}

    /** Constructor */
    public Quotes (int number) {
        if( number > 0 ) this.numberPage = number;
    }

    /** Select number page */
    public boolean setNumberPage (int number) {
        if( number > 0 ) {
            this.numberPage = number;
            return true;
        }
        return false;
    }

    /** Get number last page */
    public int getNumberPage () {
        return this.numberPage;
    }

    /** Get quotes text page */
    public String getQuotesText () {
        try {
            return getHTMLText();
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    /** Get quotes text page */
    public String getQuotesText (int numberPage) {
        setNumberPage(numberPage);
        return getQuotesText();
    }

    /** Get text page */
    private String getHTMLText () throws IOException {
        String url = this.url + getNumberPage();

        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String clearText = filterText(response.toString());
        if(clearText != null) return (clearText + "\n");
        else {
            NotFound.addNumberPage(numberPage);
            System.out.println("Error! Page not found(404).\n");
            return null;
        }
    }

    /** Clear page text */
    private String filterText (String line) {
        /** quantity excerpt text exist */
        if(line == null || !line.contains("quote__body")) return null;

        /** If ( quantity excerpt text  > 0 ) return */
        if(line.indexOf("quote__body") != line.lastIndexOf("quote__body")) return null;
        // Проверка на целостность.

        String filterLine;

        /** Get excerpt text from page */
        filterLine = line.substring(line.indexOf("<div class=\"quote__body\">") + 25, line.indexOf("<footer class=\"quote__footer\">") - 16);
        // Получаем нужный отрывок текста.

        return filterLine

        /** Clear text from " " */
                .trim()
        // Убираем лишние пробелы.

        /** Replace tag */
                .replace("<br>", "\n")
                .replace("<br />", "\n")
        // Убираем тэги и ставим отступы.

        /** Replace special tag with characters */
                .replace("&lt;","<")
                .replace("&gt;", ">")
                .replace("&quot;", "\"");
        // Преобразуем служебные символы.

    }
}